import React from 'react';
import './App.less';
import PlayingPanel from './PlayingPanel/PlayingPanel';
import ControlPanel from './ControlPanel/ControlPanel';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      answer: [],
      clearFlag: false,
    };
    this.updateAnswer = this.updateAnswer.bind(this);
    this.setClearFlag = this.setClearFlag.bind(this);
  }

  render() {
    return (
      <div className='App'>
        <PlayingPanel answer={this.state.answer} clearAll={this.state.clearFlag} />
        <ControlPanel updateHandler={this.updateAnswer} setClearFlagHandler={this.setClearFlag}/>
      </div>
    );
  }

  updateAnswer(answer) {
    this.setState({
      answer
    });
  }

  setClearFlag() {
    this.setState({
      clearFlag: true
    });
  }
}

export default App;