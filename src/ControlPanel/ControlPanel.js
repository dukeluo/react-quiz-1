import React from 'react';
import './ControlPanel.less';
import Button from '../shared/Button';
import Title from '../shared/Title';
import LetterDisplay from '../shared/LetterDisplay';
import answerGenerator from '../util/answerGenerator';

class ControlPanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      timerId: -1,
      answer: [],
      display: [],
    }
    this.createNewCard = this.createNewCard.bind(this);
    this.timer = this.timer.bind(this);
    this.showResult = this.showResult.bind(this); 
  }

  render() {
    return (
      <div className="ControlPanel">
        <Title text='new card' />
        <Button id='new-card' text='new card' handler={this.createNewCard} />
        <LetterDisplay content={this.state.display}/>
        <Button id='show-result' text='show result' handler={this.showResult} />
      </div>
    );
  }

  createNewCard() {
    let answer = answerGenerator(4).split('');

    this.setState({
      answer: answer,
      display: answer,
    });
    this.props.updateHandler(answer);
    this.props.setClearFlagHandler();
    this.timer();
  }

  timer() {
    let timerId = this.state.timerId;

    if (timerId > 0) {
      clearTimeout(timerId);
    }

    timerId = setTimeout(() => {
      this.setState({
        display: [],
      });
    }, 3000);

    this.setState({
      timerId,
    });
  }

  showResult() {
    this.setState({
      display: this.state.answer,
    });
    this.props.setClearFlagHandler();
  }
}

export default ControlPanel;