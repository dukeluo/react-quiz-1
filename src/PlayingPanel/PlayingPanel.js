import React from 'react';
import './PlayingPanel.less';
import Title from '../shared/Title';
import LetterDisplay from '../shared/LetterDisplay';
import GuessInput from '../shared/GuessInput';

const Result = (props) => {
  let successStyle = {
    color: 'green',
  };
  let failedStyle = {
    color: 'red',
  };

  if (props.isFirstTime) {
    return '';
  }
  return (
    <div className='result' style={props.isCorrect ? successStyle : failedStyle}>
      <p>{props.isCorrect ? 'success' : 'failed'}</p>
    </div>
  );
}

class PlayingPanel extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      display: [],
      isCorrect: false,
      isFirstTime: true,
    };
    this.updateDisplay = this.updateDisplay.bind(this);
    this.checkGuess = this.checkGuess.bind(this);
    this.clearAll = this.clearAll.bind(this);
  }

  render() {
    return (
      <div className='PlayingPanel'>
        <div className='your-result'>
          <Title text='your result' />
          <LetterDisplay content={this.state.display}/>
          <Result isCorrect={this.state.isCorrect} isFirstTime={this.state.isFirstTime}/>
        </div>
        <div className='input'>
          <GuessInput updateHandler={this.updateDisplay} clickHandler={this.checkGuess} inputValue={this.state.display}/>
        </div>
      </div>
    );
  }

  componentDidUpdate(prevProps) {
    if (this.props.clearAll && this.props !== prevProps) {
      this.clearAll();
    }
  }

  updateDisplay(letter, index) {
    let display = this.state.display;

    display[index] = letter;
    this.setState({
      display
    });
  }

  checkGuess() {
    let guess = this.state.display.join('');

    this.setState({
      isCorrect: guess === this.props.answer.join(''),
      isFirstTime: false,
    });
  }

  clearAll() {
    this.setState({
      display: [],
      isCorrect: false,
      isFirstTime: true,
    });
  }
}

export default PlayingPanel;