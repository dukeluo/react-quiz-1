import React from 'react';
import './GuessInput.less';
import Button from './Button';
import Title from './Title';

class GuessInput extends React.Component {
  constructor(props) {
    super(props);
    this.changeHandler = this.changeHandler.bind(this);
  }

  render() {
    return (
      <div className="GuessInput">
        <Title text='guess card' />
        <input type="text" maxLength='4' onChange={this.changeHandler} value={this.props.inputValue.join('')}/>
        <Button text='guess' handler={this.props.clickHandler}/>
      </div>
    );
  }

  changeHandler(event) {
    let letters = event.target.value.split('');
    let input = this.props.inputValue;

    for (let i = 0; i < 4; i++) {
      if (letters[i] !== input[i]) {
        input[i] = letters[i];
        this.setState({
          input
        });
        this.props.updateHandler(input[i], i);
      }
    }
  }
}

export default GuessInput;