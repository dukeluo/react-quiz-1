import React from 'react';
import './LetterDisplay.less';

const LetterDisplay = (props) => {
    return (
        <div className='LetterDisplay'>
            <span>{!props.content[0] ? String.fromCharCode(160) : props.content[0]}</span>
            <span>{!props.content[1] ? String.fromCharCode(160) : props.content[1]}</span>
            <span>{!props.content[2] ? String.fromCharCode(160) : props.content[2]}</span>
            <span>{!props.content[3] ? String.fromCharCode(160) : props.content[3]}</span>
        </div>
    );
}

export default LetterDisplay;