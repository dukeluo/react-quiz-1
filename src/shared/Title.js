import React from 'react';
import './Title.less';

const Title = (props) => {
  return (
    <div className="Title">
      <h1>{props.text}</h1>
    </div>
  );
}

export default Title;